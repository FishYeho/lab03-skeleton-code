package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		//TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		//TODO:  increment the variables' values when their corresponding lifecycle methods get called.
		public int onCreateCounter = 0;
		public int onStartCounter = 0;
		public int onResumeCounter = 0;
		public int onPauseCounter = 0;
		public int onStopCounter = 0;
		public int onDestroyCounter = 0;
		public int onRestartCounter = 0;

		TextView etCreate;
		TextView etStart;
		TextView etResume;
		TextView etPause;
		TextView etRestart;
		TextView etDestroy;
		TextView etStop;

		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);
			
			//Log cat print out
			Log.i(TAG, "onCreate called");
			
			//TODO: update the appropriate count variable & update the view
			etCreate = (TextView) findViewById(R.id.create);
			etStart = (TextView) findViewById(R.id.start);
			etResume = (TextView) findViewById(R.id.resume);
			etPause = (TextView) findViewById(R.id.pause);
			etRestart = (TextView) findViewById(R.id.restart);
			etDestroy = (TextView) findViewById(R.id.destroy);
			etStop = (TextView) findViewById(R.id.stop);
			//TODO: CONTINUE HERE
			onCreateCounter++;
			etCreate.setText("onCreate calls: " + onCreateCounter);
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();

			//Log cat print out
			Log.i(TAG, "onStart called");
			//TODO:  update the appropriate count variable & update the view
			onStartCounter++;
			etStart.setText("onStart calls: " + onStartCounter);
		}

	    // TODO: implement 5 missing lifecycle callback methods
	    @Override
		public void onResume() {
			super.onResume();

			//Log cat print out
			Log.i(TAG, "onResume called");
			onResumeCounter++;
			etResume.setText("onResume calls: " + onResumeCounter);
		}

		@Override
		public void onPause() {
			super.onPause();

			//Log cat print out
			Log.i(TAG, "onPause called");
			onPauseCounter++;
			etPause.setText("onPause calls: " + onPauseCounter);
		}

		@Override
		public void onStop() {
			super.onStop();

			//Log cat print out
			Log.i(TAG, "onStop called");
			onStopCounter++;
			etStop.setText("onStop calls: " + onStopCounter);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();

			//Log cat print out
			Log.i(TAG, "onDestroy called");
			onDestroyCounter++;
			etDestroy.setText("onDestroy calls: " + onDestroyCounter);
		}

		@Override
		public void onRestart() {
			super.onRestart();

			//Log cat print out
			Log.i(TAG, "onRestart called");
			onRestartCounter++;
			etRestart.setText("onRestart calls: " + onRestartCounter);
		}

	    // Note:  if you want to use a resource as a string you must do the following
	    //  getResources().getString(R.string.stringname)   returns a String.

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			//TODO:  save state information with a collection of key-value pairs & save all  count variables
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}
}
